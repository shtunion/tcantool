﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using tcantool.util;

namespace tcantool.view
{
    public partial class MainForm : Form
    {
        //打开标识符
        private uint openFlag = 0;
        private uint deviceType = 0;
        private uint deviceIndex = 0;
        private uint canIndex = 0;

        private Timer timer;

        private List<uint> deviceTypeList = new List<uint>(2);


        VCI_CAN_OBJ[] canReceiveBuffer = new VCI_CAN_OBJ[1000];

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            deviceTypeList.Add(0);
            deviceTypeList.Add(0);

            //设备类型下拉框初始化
            int index = deviceTypeComboBox.Items.Add("DEV_USBCAN");
            deviceTypeList[index] = 3;
            index = deviceTypeComboBox.Items.Add("DEV_USBCAN2");
            deviceTypeList[index] = 4;
            deviceTypeComboBox.SelectedIndex = 0;

            deviceIndexComboBox.Items.Add(0);
            deviceIndexComboBox.Items.Add(1);
            deviceIndexComboBox.SelectedIndex = 0;

            canIndexComboBox.Items.Add(0);
            canIndexComboBox.Items.Add(1);
            canIndexComboBox.SelectedIndex = 0;

            //创建一个定时器
            timer = new Timer();
            timer.Tick += new System.EventHandler(TimerHandler);
            timer.Interval = 100;
            //new System.Windows.Forms.Timer(this.components);
            timer.Start();

        }

        unsafe private void TimerHandler(object sender, EventArgs e)
        {
            uint ret = 0;

            ret = ControlCanUtil.VCI_Receive(deviceType, deviceIndex, canIndex, ref canReceiveBuffer[0], 1000, 100);
            //设备未初始化
            if (ret == 0xFFFFFFFF) ret = 0;

            String str = "";
            for (UInt32 i = 0; i < ret; i++)
            {
                //VCI_CAN_OBJ obj = (VCI_CAN_OBJ)Marshal.PtrToStructure((IntPtr)((UInt32)pt + i * Marshal.SizeOf(typeof(VCI_CAN_OBJ))), typeof(VCI_CAN_OBJ));

                str = "接收到数据: ";
                str += "  帧ID:0x" + System.Convert.ToString(canReceiveBuffer[i].ID, 16);
                str += "  帧格式:";
                if (canReceiveBuffer[i].RemoteFlag == 0)
                    str += "数据帧 ";
                else
                    str += "远程帧 ";
                if (canReceiveBuffer[i].ExternFlag == 0)
                    str += "标准帧 ";
                else
                    str += "扩展帧 ";

                //////////////////////////////////////////
                if (canReceiveBuffer[i].RemoteFlag == 0)
                {
                    str += "数据: ";
                    byte len = (byte)(canReceiveBuffer[i].DataLen % 9);
                    byte j = 0;
                    fixed (VCI_CAN_OBJ* canReceiveBuffer1 = &canReceiveBuffer[i])
                    {
                        if (j++ < len)
                            str += " " + System.Convert.ToString(canReceiveBuffer1->Data[0], 16);
                        if (j++ < len)
                            str += " " + System.Convert.ToString(canReceiveBuffer1->Data[1], 16);
                        if (j++ < len)
                            str += " " + System.Convert.ToString(canReceiveBuffer1->Data[2], 16);
                        if (j++ < len)
                            str += " " + System.Convert.ToString(canReceiveBuffer1->Data[3], 16);
                        if (j++ < len)
                            str += " " + System.Convert.ToString(canReceiveBuffer1->Data[4], 16);
                        if (j++ < len)
                            str += " " + System.Convert.ToString(canReceiveBuffer1->Data[5], 16);
                        if (j++ < len)
                            str += " " + System.Convert.ToString(canReceiveBuffer1->Data[6], 16);
                        if (j++ < len)
                            str += " " + System.Convert.ToString(canReceiveBuffer1->Data[7], 16);
                    }
                }

                Console.WriteLine(str);
                //listBox_Info.Items.Add(str);
                //listBox_Info.SelectedIndex = listBox_Info.Items.Count - 1;
            }
        }


        private void startButton_Click(object sender, EventArgs e)
        {
            //以下两行为多卡同机测试代码，用来获取序列号与对应的设备索引号，单卡可以不使用。
            VCI_BOARD_INFO[] vbi2 = new VCI_BOARD_INFO[50];
            uint num1 = ControlCanUtil.VCI_FindUsbDevice2(ref vbi2[0]);


            if (openFlag == 1)
            {
                ControlCanUtil.VCI_CloseDevice(deviceType, deviceIndex);
                openFlag = 0;
            }
            else
            {
                //打开设备
                deviceType = deviceTypeList[deviceTypeComboBox.SelectedIndex];
                deviceIndex = (UInt32)deviceIndexComboBox.SelectedIndex;
                canIndex = (UInt32)canIndexComboBox.SelectedIndex;
                if (ControlCanUtil.VCI_OpenDevice(deviceType, deviceIndex, 0) == 0)
                {
                    MessageBox.Show("打开设备失败");
                    return;
                }
                openFlag = 1;

                //初始化can
                VCI_INIT_CONFIG config = new VCI_INIT_CONFIG();
                config.AccCode = System.Convert.ToUInt32("0x00000000", 16);
                config.AccMask = System.Convert.ToUInt32("0xFFFFFFFF", 16);
                config.Timing0 = System.Convert.ToByte("0x00", 16);
                config.Timing1 = System.Convert.ToByte("0x1c", 16);
                config.Filter = 1;//接收全部
                config.Mode = 0;//正常模式
                ControlCanUtil.VCI_InitCAN(deviceType, deviceIndex, canIndex, ref config);


                //启动设备
                ControlCanUtil.VCI_StartCAN(deviceType, deviceIndex, canIndex);


                
                /*
                VCI_INIT_CONFIG config = new VCI_INIT_CONFIG();
                config.AccCode = System.Convert.ToUInt32("0x" + textBox_AccCode.Text, 16);
                config.AccMask = System.Convert.ToUInt32("0x" + textBox_AccMask.Text, 16);
                config.Timing0 = System.Convert.ToByte("0x" + textBox_Time0.Text, 16);
                config.Timing1 = System.Convert.ToByte("0x" + textBox_Time1.Text, 16);
                config.Filter = (Byte)(comboBox_Filter.SelectedIndex + 1);
                config.Mode = (Byte)comboBox_Mode.SelectedIndex;
                VCI_InitCAN(deviceType, deviceIndex, canIndex, ref config);
                */
            }
            //buttonConnect.Text = openFlag == 1 ? "断开" : "连接";
            //timer_rec.Enabled = openFlag == 1 ? true : false;
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (openFlag == 1)
            {
                ControlCanUtil.VCI_CloseDevice(deviceType, deviceIndex);
            }
        }
    }
}
