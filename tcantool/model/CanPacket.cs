﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tcantool.model
{
    class CanPacket
    {
        public int CanId { get; set; }

        public byte[] Data { get; set; }


        public CanPacket()
        {
            Data = new byte[8];
        }

    }
}
